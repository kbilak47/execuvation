#!/usr/bin/env python
import array
import itertools
import sys
'''
		Hurricane Execuvation Problem
		Programmers: Jonathan Himmelsbach and Kyle Bilak
		Dev Status: Active(not complete)
		Course: CSCI B350
'''
# initialize variables
i = 0
j = 0
k = 0
l = 0

population = [2,4,3,1,2,1,3,3,3,1,2,2,2,4,2,4,4,3,4,1,4,1,3,2,1,3,2,1,3,4,4,3,4,3,2,1,4,4,4,3,4,4,3,2,2,1,2,4,2,1,4,1,3,2,2,4,4,2,2,3,1,4,3,3,1,3,1,3,1,1,3,2,3,2,3,3,3,2,
              2,1,2,4,1,3,1,1,3,1,4,1,1,4,3,4,2,3,3,3,2,2,2,4,4,3,1,3,2,3,2,3,2,2,1,1,3,2,2,2,2,1,3,3,2,1,4,4,4,3,4,1,3,3,2,1,3,3,4,4,4,2,2,4,2,4,2,2,2,3,2,1,3,3,3,3,2,1,
	          1,4,3,4,3,4,1,2,1,4,4,2,3,3,4,1,3,1,1,2,1,4,1,1,2,3,2,4,3,3,1,1,4,4,1,2,1,4,4,2,3,2,2,3,2]


shelterName = ["B","C","D","E"] # shelter list
shelterCapacity= [200,200,200,200] # number of people each shelter holds

routeName = ["B1","B2","C1","C2","D1","D2","E1","E2"] # different routes to each shelter 
routeCapacity = [125,75,110,90,100,100,80,120] # amount of people each route can hold
routeTime = [83,45,72,65,68,42,91,109] # amount of time each route takes
ifcapacity = len(population) - 1


while i < ifcapacity:
	maxRouteTime = 0
	maxRouteName = " "
	routeToShelter = " "
	ifShelterCapacity = len(shelterName) - 1
	for j in ifShelterCapacity: # have to use range function
	# goes through element in the shelter array
			if shelterCapacity[j] - population[i] > 0:
				for l in len(routeName) -1:
				if shelterName[j] + "%" in routeName[l]:       # contains the letter of shelterName
					routeToShelter[l] = routeName[l]		   # index of the route in the RouteName Array
					break
					for  k in len(routeToShelter) -1:
								if routeCapacity[routeToShelter[k]] - 1 > 0:
									if routeTime[routeToShelter[k]] > maxRouteName:
										maxRouteName = routeName[routeToShelter[k]]
										maxRouteTime = maxRouteTime[routeToShelter[k]]
										++j
										++l
										++k

	routeCapacity[routeToShelter[k]] = routeCapacity[routeToShelter[k]] - 1
	shelterCapacity[j] = shelterCapacity[j] - population[i]
	ifcapacity = len(popualtion) - 1
	print(maxRouteName + " " + shelterName[j]) # print routeToShelter
	print("Program has ended. Bye!") # test print statement
	sys.exit(1) # exit program nicely
